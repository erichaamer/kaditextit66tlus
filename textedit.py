import tkinter as tk
import re
import operator
import numpy as np

def readdata(filename):
	with open(filename) as f:
		lines = f.readlines()
	#return "\n".join(lines).replace("-\n","").replace("\n"," ")
	return "\n".join(lines).replace("\n",chr(255))
def writedata(filename,text):
	with open(filename,'w') as f:
		f.write(text)
	
def findandremove(text, symbols):
	highlight = []
	remove = []
	for section in symbols:
		start = [i for i in findall(section[0], text)]
		end = [i for i in findall(section[1], text)]
		iterator = 0
		startValueTemp = 0
		while True:
			if (len(start)==0 or len(end)==0):
				if (iterator!=0):
					highlight.append(["1."+str(startValueTemp),"end"])
					remove.append([startValueTemp,-1])
				break
			if (start[0] < end[0]):
				if (iterator==0):
					startValueTemp = start[0]
				iterator+=1
				del start[0]
			else:
				iterator-=1
				if (iterator==0):
					highlight.append(["1."+str(startValueTemp),"1."+str(end[0]+1)])
					remove.append([startValueTemp,end[0]+1])
				if (iterator<0):
					iterator=0	
				del end[0]
	return [text, highlight, remove]


def findall(p, s):
    '''Yields all the positions of
    the pattern p in the string s.'''
    i = s.find(p)
    while i != -1:
        yield i
        i = s.find(p, i+1)

def removefromtext(text,remove):
	iterator = 0
	remove = sorted(remove, key=operator.itemgetter(0))
	remove.reverse()
	while iterator < len(remove):
		text = text[:remove[iterator][0]]+text[remove[iterator][1]:]
		iterator+=1
	return text

def replaceremovefromtext(text,remove):
	iterator = 0
	remove = sorted(remove, key=operator.itemgetter(0))
	remove.reverse()
	mask = np.ones(len(text), dtype=bool)
	while iterator < len(remove):
		mask[remove[iterator][0]:remove[iterator][1]]=False
		iterator+=1
	#print(mask)
	text = ''.join(np.array(list(text))[mask]).replace(chr(255),"\n")
	return text

	
#window = tk.Tk()
def OnResize(event):
	window.config(width=event.width, height=event.height)
	winfo_width.set(str(root.winfo_width()))
	
def display(text,highlight):
	window = tk.Tk()

	scrollbar = tk.Scrollbar(window)
	scrollbar.pack(side=tk.RIGHT, fill=tk.Y)
	textbox = tk.Text(window, height=500, width=500)
	textbox.pack()
	textbox.insert(tk.END, text)
	textbox.config(yscrollcommand=scrollbar.set)
	scrollbar.config(command=textbox.yview)

	# adding a tag to a part of text specifying the indices
	for mark in highlight:
		textbox.tag_add("delete", mark[0],mark[1])
	textbox.tag_config("delete", background="black", foreground="yellow")

	
	#def OnResize(event):
	#	window.config(width=event.width, height=event.height)
	#	winfo_width.set(str(window.winfo_width()))
	#winfo_width = tk.StringVar()
	#winfo_width.set(str(window.winfo_width()))
	#window.bind("<Configure>", OnResize)

	window.mainloop()
	

def main():
	text = readdata('in.txt')
	symbols = [["(",")"],['"','"'],["{","}"]]
	[text,highlight, remove] = findandremove(text, symbols)
	cleantext = replaceremovefromtext(text,remove)
	print(cleantext,remove)
	writedata('out.txt',cleantext)
	display(text.replace(chr(255)," "),highlight)
	
if __name__ == "__main__":
    main()