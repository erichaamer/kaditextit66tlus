import tkinter as tk
import re
import operator
import numpy as np

#with open("Vessart_MA_2021.txt","r", encoding="utf8") as f:
#	lines = f.readlines()
#	text = "\n".join(lines)

def breaktext(text):
	abstract = re.finditer('\n*abstract\n|\n*abstrakt\n', text, flags=re.IGNORECASE)
	#intro = re.search('^\\s*[0-9]*\\s*introduction', text, flags=re.IGNORECASE)
	#intro = re.search('[0-9]+\n*introduction', text, flags=re.IGNORECASE)
	intro = re.search('[0-9]+\n*introduction\n', text, flags=re.IGNORECASE)


	abstract_start_end = [0,0]
	for match in abstract:
		if abstract_start_end[0]==0:
			abstract_start_end[0]=match.start()
		elif abstract_start_end[1]==0:
			abstract_start_end[1]=match.start()

	if abstract_start_end[1]==0:
		ABSTRACT = text[abstract_start_end[0]:]
		abstract_end = re.search('\n[0-9]\n', ABSTRACT, flags=re.IGNORECASE)
		#print(abstract_end)
		abstract_start_end[1] = abstract_end.start()
		ABSTRACT = ABSTRACT[:abstract_start_end[1]]
		abstract_start_end[1] += abstract_start_end[0]
	#else:
	ABSTRACT = text[abstract_start_end[0]:abstract_start_end[1]]
		
	#print(ABSTRACT)
	#print(abstract_start_end)


	references = re.finditer('references.*|list of references.*|the list of references.*|\nlist of references|\nreferences|\nthe list of references|\nprimary sources', text, flags=re.IGNORECASE)
	for match in references:
		pass

#conclusion = re.search('[0-9]+\n*conclusion\n', text, flags=re.IGNORECASE)
	clean_start_end = [intro.start(),match.start()]
	CLEAN = text[clean_start_end[0]:clean_start_end[1]]
	
	return [abstract_start_end,clean_start_end,ABSTRACT,CLEAN]

def removeadditional(text):
	pagenumbers = []
	for match in re.finditer('\n[0-9]+\n|\n[0-9]+$', text, flags=re.IGNORECASE):
		pagenumbers.append([match.start(),match.end()])

	tablerows = []
	for match in re.finditer('.*[ ]{4}.*', text, flags=re.IGNORECASE):
		tablerows.append([match.start(),match.end()])
		
	quotations = []
	#for match in re.finditer('\[([^\]]|\n)*\]|\{([^\}]|\n)*\}|\(([^\)]|\n)*\)|\"([^\"]|\n)*\"', text, flags=re.IGNORECASE):
	#	quotations.append([match.start(),match.end()])
	
	
	#TEMPLATE =  'START([^END]|\n)*[ ]+([^END]|\n)*END|START[0-9]+END'
	TEMPLATE =  'START([^END]|\n)*[ \n]+([^END]|\n)*[ \n]+([^END]|\n)*END|START([^END]|\n)*[0-9]+([^END]|\n)*END'
	symbols = [["\(","\)"],['\"','\"'],["\{","\}"],["\“","\”"]]
	for case in symbols:
		#print(case)
		for match in re.finditer(TEMPLATE.replace("START",case[0]).replace("END",case[1]),text, flags=re.IGNORECASE):
			quotations.append([match.start(),match.end()])
		
	return [pagenumbers,tablerows,quotations]
	
def removetitles(text):
	titles = []
	#for match in re.finditer('\n([A-Z0-9]|[ !"#$%&*+,./:;<=>?@\^_`’|~-])+\n', text):
	for match in re.finditer('\n[^a-z\n]+\n', text):
		titles.append([match.start(),match.end()])
	for match in re.finditer('\n[1-9]+\.[1-9]+.+\n', text):
		titles.append([match.start(),match.end()])
	
	return [titles]

def nextlinepos(text):
	nextlines = []
	for match in re.finditer('\n', text, flags=re.IGNORECASE):
		nextlines.append(match.start())
	nextlines.append(len(text))
	return nextlines

def formattotkinter(indexlist,listofnextline):
	tkinterlist = []
	#for value in indexlist:
	#	tkinterlist.append(["1."+str(value[0]),"1."+str(value[1])])
	
	#nextlineID = 0
	for value in indexlist:
		nextlineID = 0
		while listofnextline[nextlineID] < value[0]:
			nextlineID+=1
		START = str(nextlineID+1)+"."+str(value[0]-listofnextline[nextlineID-1]-1)
		
		if value[1] > 0:
			while listofnextline[nextlineID] < value[1]:
				nextlineID+=1
			END = str(nextlineID+1)+"."+str(value[1]-listofnextline[nextlineID-1]-1)
		else:
			END = "end"
		tkinterlist.append([START,END])
		
	return tkinterlist



def clearexceptions(text, deletions, exceptions):
	for i in range(len(deletions)):
		if len(deletions[i])<2:
			continue
		select = text[deletions[i][0]:deletions[i][1]]
		#print("vals:",select)
		for exception in exceptions:
			if exception in select:
				deletions[i] = []
				break
	return [x for x in deletions if x != []]


def readdata(filename):
	with open(filename,encoding="utf8") as f:
		lines = f.readlines()
	#return "\n".join(lines).replace("-\n","").replace("\n"," ")
	return "".join(lines)#.replace("\n",chr(255))
def writedata(filename,text):
	with open(filename,'w',encoding="utf8") as f:
		f.write(text)
def readexceptions(filename):
	with open(filename,encoding="utf8") as f:
		lines = f.readlines()
	return lines

def findandremove(text, symbols):
	remove = []
	for section in symbols:
		start = [i for i in findall(section[0], text)]
		end = [i for i in findall(section[1], text)]
		iterator = 0
		startValueTemp = 0
		while True:
			if (len(start)==0 or len(end)==0):
				if (iterator!=0):
					remove.append([startValueTemp,-1])
				break
			if (start[0] < end[0]):
				if (iterator==0):
					startValueTemp = start[0]
				iterator+=1
				del start[0]
			else:
				iterator-=1
				if (iterator==0):
					remove.append([startValueTemp,end[0]+1])
				if (iterator<0):
					iterator=0	
				del end[0]
	return [text, remove]


def findall(p, s):
    '''Yields all the positions of
    the pattern p in the string s.'''
    i = s.find(p)
    while i != -1:
        yield i
        i = s.find(p, i+1)

def removefromtext(text,remove):
	iterator = 0
	remove = sorted(remove, key=operator.itemgetter(0))
	remove.reverse()
	while iterator < len(remove):
		text = text[:remove[iterator][0]]+text[remove[iterator][1]:]
		iterator+=1
	return text

def replaceremovefromtext(text,remove):
	iterator = 0
	remove = sorted(remove, key=operator.itemgetter(0))
	remove.reverse()
	mask = np.ones(len(text), dtype=bool)
	while iterator < len(remove):
		mask[remove[iterator][0]:remove[iterator][1]]=False
		iterator+=1
	#print(mask)
	text = ''.join(np.array(list(text))[mask]).replace(chr(255),"\n")
	return text


	
#window = tk.Tk()
def OnResize(event):
	window.config(width=event.width, height=event.height)
	winfo_width.set(str(root.winfo_width()))
	
def display(text,highlight):
	window = tk.Tk()

	scrollbar = tk.Scrollbar(window)
	scrollbar.pack(side=tk.RIGHT, fill=tk.Y)
	textbox = tk.Text(window, height=500, width=300,wrap="word")#,font=("Arial",12))
	textbox.pack()
	textbox.insert(tk.END, text)
	textbox.config(yscrollcommand=scrollbar.set)
	scrollbar.config(command=textbox.yview)

	# adding a tag to a part of text specifying the indices
	for mark in highlight:
		textbox.tag_add("delete", mark[0],mark[1])
	textbox.tag_config("delete", background="black", foreground="yellow")

	
	#def OnResize(event):
	#	window.config(width=event.width, height=event.height)
	#	winfo_width.set(str(window.winfo_width()))
	#winfo_width = tk.StringVar()
	#winfo_width.set(str(window.winfo_width()))
	#window.bind("<Configure>", OnResize)

	window.mainloop()
	

def main(filename = 'Vessart_MA_2021.txt'):

	exceptions_file = 'exceptions.txt'
	exceptions = readexceptions(exceptions_file)
	
	text = readdata(filename)
	#display(text.replace("\n","\n"),[])
	
	[abstract_start_end,clean_start_end,ABSTRACT,CLEAN] = breaktext(text)
	nextlines = nextlinepos(text)
	highlight = formattotkinter([abstract_start_end,clean_start_end],nextlines)

	display(text,highlight)
	text = ABSTRACT+"\n"+CLEAN
	
	#symbols = [["(",")"],['"','"'],["{","}"]]
	#[text, remove] = findandremove(text, symbols)
	[pagenumbers,tablerows,quotations] = removeadditional(text)
	nextlines = nextlinepos(text)
	#highlight = formattotkinter(remove+pagenumbers+tablerows,nextlines)
	quotations = clearexceptions(text, quotations, exceptions)
	highlight = formattotkinter(quotations,nextlines)
	display(text,highlight)
	text = replaceremovefromtext(text,quotations)
	
	[pagenumbers,tablerows,quotations] = removeadditional(text)
	nextlines = nextlinepos(text)
	highlight = formattotkinter(pagenumbers+tablerows,nextlines)
	display(text,highlight)
	text = replaceremovefromtext(text,pagenumbers+tablerows)
	
	text = text.replace("\n\n","\n").replace("\n\n","\n").replace("\n\n","\n").replace("\n\n","\n").replace("\n\n","\n")

	#[titles] = removetitles(text)
	#nextlines = nextlinepos(text)
	#highlight = formattotkinter(titles,nextlines)
	#display(text,highlight)
	#text = replaceremovefromtext(text,titles)
	
	#cleantext = replaceremovefromtext(text,quotations+pagenumbers+tablerows)
	#print(cleantext,remove)
	writedata('out\\'+filename,text)
	display(text,[])
	
if __name__ == "__main__":
    main()